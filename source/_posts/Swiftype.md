﻿---
title: Swiftype
date: 2017-01-18 10:12:54
tags: Swiftype
---
### Swiftype站内搜索
![](https://www.news47ell.com/wp-content/uploads/2015/03/swiftype-logo.png)
>本站已经添加站内搜索
用的是Swiftype

<!--more-->
### 安装

[Swiftype.com](https://swiftype.com/)
1、注册账户
2、创建搜索引擎
3、定制与激活搜索
4、获取 Key
5、更新主题配置