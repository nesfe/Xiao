---
title: nginx虚拟主机配置
date: 2017-03-24 14:27:22
tags: nginx
---
>nginx虚拟主机配置

nginx/conf/nginx.conf

``` json
server {
	listen 80;
	server_name a.com;# 域名1
	location / {
		proxy_pass http://localhost:8080; #本地端口
	}
    }

server {
	listen 80;
	server_name b.com;# 域名2
	location / {
		proxy_pass http://localhost:3000; #本地端口
	}
    }

server {
	listen 80;
	server_name c.com;# 域名3
	location / {
		proxy_pass http://localhost:2017; #本地端口
	}
    }
```

<!--more-->