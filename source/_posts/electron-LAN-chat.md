---
title: electron-LAN-chat
date: 2017-05-18 20:31:00
tags: electron
---

#### Electron LAN Chat

--------------------------------

✨ 学习项目：局域网聊天工具 ✨

--------------------------------

✨ 界面 ✨ 

![](http://i1.piimg.com/588926/69a1b8bf510b031b.png)

<!--more-->

✨ 工具 ✨
- React
- JQuery
- Redux
- WebPack
- eslint
- electron
- asar
- node > net

✨ 实现功能 ✨
- 添加局域网IP
- 局域网内通讯
- 发送表情 (暂未实现)

✨ 安装 ✨

``` bash
git clone https://github.com/nesfe/electron-LAN-chat.git
cd electron-LAN-chat
npm install
npm run dev
```