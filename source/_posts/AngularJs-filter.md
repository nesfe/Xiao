﻿---
title: AngularJs Filter 九种过滤器 + 自定义
date: 2017-02-22 15:13:50
tags: angular
---
>过滤器可以使用一个管道字符（|）添加到表达式和指令中。
![](http://www.runoob.com/wp-content/uploads/2014/06/angular.jpg)


<!--more-->
### currency
>把一个数字格式化成货币模式（如$1,234.56）。当没有提供任何货币符号时，默认使用当前区域的符号。

HTML：
``` html
{{ currency_expression | currency:symbol:fractionSize}}
```
JS:
``` js
$filter(“currency”)(amount,symbol,fractionSize);
```
amount：数值，过滤的值。
symbol：字符串，要显示的货币符号或标识符。
fractionSize：数值，整数的小数位数，默认为当前的最大位数。


### date
>基于需要的格式将时间格式化成字符串。

HTML：
``` html
{{date_expression | date:format:timezone}}
```
JS:
``` js
$filter(“date”)(date,format,timezone);
```
date：格式化为日期的日期对象。如果没有指定输入的时区字符串，时间则是当地时间。
format：格式规则/格式。
timezone：时区。


### filter
>从数组中选出一个子集，并将其作为一个新数组返回。

HTML：
``` html
{{filter_expression | filter:expression:comparator}}
```
JS:
``` js
$filter(“filter”)(array,expression,comparator);
```
array：被过滤的数组。
expression：字符串/对象/函数，用于从数组中选择数据的判断表达式。使用$可以匹配任何字段。
comparator：函数/Boolean/undefined，用于确定预期的值（从filter表达式返回）和实际的值（数组中的对象）进行比较，应视为匹配。function(actual,expected);

### json
>允许将一个javascript对象转换为json字符串。

HTML：
``` html
{{json_expression | json:spacing}}
```
JS:
``` js
$filter(“json”)(object,spacing);
```
object：过滤的对象。
spacing：每个缩进的空格数，默认为2。

### limitTo 
>创建一个只包含指定数目元素的数组或字符串。元素是按指定的值和符号（+或-）从数组、字符串或数字的开头或结尾获取的。如果输入一个数字，则转换为字符串。

HTML：
``` html
{{limitTo_expression | limitTo:limit:begin}}
```
JS:
``` js
$filter(“limitTo”)(input,limit,begin);
```
input：限制的数组，字符串，数字。
limit：限制的长度。
begin：限制长度开始的位置（根据索引）。

### lowercase
>将字符串转换为小写。

HTML：
``` html
{{lowercase_expression | lowercase}}
```
JS:
``` js
$filter(“lowercase”)(input);
```
input：过滤的字符串。

### number
>将数值转换为文本。
如果输入是null或undefined，那么其将被返回。如果输入的是无穷（正无穷/负无穷），将会返回无穷大的符号“∞”。如果输入不是一个数字，返回一个空字符串。

HTML：
``` html
{{number_expression | number:fractionSize}}
```
JS:
``` js
$filter(“number”)(number,fractionSize);
```
number：转换的数值。
fractionSize：数值，整数的小数位数，默认为当前的最大位数。在默认的区域设置的情况下这个数值是3。

### orderBy
通过判断表达式将指定的数组进行排序。它是按字符串的字母顺序和数值的数字排序的。
注意：如果你发现数字没有按预期排序，请确保它们实际上是被保存为数字而不是字符串。

HTML：
``` html
{{orderBy_expression | orderBy:expression:reverse}}
```
JS:
``` js
$filter(“orderBy”)(array,expression,reverse);
```
array：排序的数组。
expression：字符串/函数/数组，用来确定元素顺序的表达式。
reverse：boolean，颠倒数组的顺序。默认为false。

### uppercase
>将字符串转换为大写。

HTML：
``` html
{{uppercase_expression |uppercase}}
```
JS:
``` js
JS：$filter(“uppercase”)(input);
```
Input：过滤的字符串。

### 自定义filter
JS:
``` js
angular.module("myApp",[]).filter("filterName",["dependancy",function(dependancy){
        return function(value){
            //your code   return the data which passed filter（返回过滤后的数据）
	    return isNaN(value)?"NaN":value.toString(16);
            //转换成16进制
        };
    }]);
```
filterName：过滤器名称。
dependency：注入的服务。
value：需要过滤的数据。
HTML：
``` html
num=255
{{ num | filterName}}
//输出 ff
```

END!