﻿---
title: webpack分离打包css为单独文件
date: 2017-01-12 10:09:34
tags:
---
>本文继续延续上一文
由于把所有的js和css文件打包成common.js，很臃肿，所以为此分离css
css打包成app.css单独引入

<!--more-->
其实分离操作很简单，就几个步骤
### 安装需要的插件
详细：[extract-text-webpack-plugin](https://www.npmjs.com/package/extract-text-webpack-plugin) - Demo
``` js
$ npm install extract-text-webpack-plugin --save
```

### 创建extract-text-webpack-plugin
``` js
var ExtractTextPlugin = require("extract-text-webpack-plugin");
```

### 添加一条plugins
``` js
new ExtractTextPlugin("app.css"),
```

### 修改loaders
``` js
{test: /\.css$/, loader:  ExtractTextPlugin.extract("style-loader","css-loader")},
```
这样就会把所有css打包成app.css
存放在js文件夹下

### 配置文件
``` js
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var path = require('path');
var webpack = require('webpack');
var fs = require('fs');
var uglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var srcDir = path.resolve(process.cwd(), 'src');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

//获取js文件夹所有js文件
function getEntry() {
    var jsPath = path.resolve(srcDir, 'js');
    var dirs = fs.readdirSync(jsPath);
    var matchs = [], files = {};
    dirs.forEach(function (item) {
        matchs = item.match(/(.+)\.js$/);
        if (matchs) {
            files[matchs[1]] = path.resolve(srcDir, 'js', item);
        }
    });
    return files;
}
module.exports = {
    cache: true,
    devtool: "source-map",
    entry: getEntry(),
    output: {
        path: path.join(__dirname, "dist/js/"),
        publicPath: "dist/js/",
        filename: "[name].js",
        chunkFilename: "[chunkhash].js"
    },
    module: {
        loaders: [
            {test: /\.css$/, loader:  ExtractTextPlugin.extract("style-loader","css-loader")},
            {test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=50000&name=[path][name].[ext]'},
            {
                test: require.resolve(srcDir + '/js/lib/angular.min.js'),
                loader: "exports?window.angular"
            }//对于非模块化的库，可以通过exports插件暴露出全局变量，如angular
        ],
        //配置webpack直接忽略不解析该文件的依赖关系。对于比较大的库很有用，如reactjs.主要提高打包速度。
        noParse: [
            srcDir + '/js/lib/angular.min.js'
        ]
    },
    resolve: {
        alias: {
            angular: srcDir + "/js/lib/angular.min.js",
            jquery: srcDir + "/js/lib/jquery.min.js",
            core: srcDir + "/js/core",
            ui: srcDir + "/js/ui"
        }
    },
    plugins: [
        //提供全局的变量，在模块中使用无需用require引入
        new webpack.ProvidePlugin({
            angular: "angular",
            jquery: "jquery",
            jQuery:"jquery",
            $: "jquery"
        }),
        //相同代码打包成common.js
        new CommonsChunkPlugin('common.js'),
        //css打包成app.css
        new ExtractTextPlugin("app.css"),
        new uglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};
```