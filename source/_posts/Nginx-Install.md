---
title: Linux系统下安装Nginx
date: 2017-01-03 13:50:26
tags: [Linux,Nginx]
---
### 什么是Nginx?
>Nginx ("engine x") 是一个高性能的 HTTP 和 反向代理 服务器，也是一个 IMAP/POP3/SMTP 代理服务器，在高连接并发的情况下Nginx 是 Apache 服务器不错的替代品.其特点是占有内存少，并发能力强，事实上nginx的并发能力确实在同类型的网页服务器中表现较好.目前中国大陆使用nginx网站用户有：新浪、网易、 腾讯，另外知名的微网志Plurk也使用nginx。

<!--more-->

Nginx 作为 负载均衡 服务器：
Nginx 既可以在内部直接支持 Rails 和 PHP 程序对外进行服务，也可以支持作为 HTTP 代理服务器对外进行服务。 Nginx 采用 C 进行编写， 不论是系统资源开销还是 CPU 使用效率都比 Perlbal 要好很多。作为邮件代理服务器：
Nginx 同时也是一个非常优秀的邮件代理服务器（最早开发这个产品的目的之一也是作为邮件代理服务器）， Last.fm 描述了成功并且美妙的使用经验。Nginx 是一个安装非常简单，配置文件非常简洁（还能够支持perl语法）， Bugs非常少的服务器： Nginx 启动特别容易，并且几乎可以做到 7*24 不间断运行，即使运行数个月也不需要重新启动。 你还能够不间断服务的情况下进行软件版本的升级。

### 安装前的准备
正式开始前，编译环境gcc g++ 开发库之类的需要提前装好
如果已经安装，可以跳过
``` bash
//安装 make
$ yum -y install gcc automake autoconf libtool make

//安装 g++
$ yum install gcc gcc-c++
```

### Nginx的安装
模块依赖性Nginx需要依赖下面3个包
gzip 模块需要 zlib 库
rewrite 模块需要 pcre 库
ssl 功能需要 openssl 库

下载所需的程序包
openssl-fips-2.0.2.tar.gz
zlib-1.2.7.tar.gz
pcre-8.21.tar.gz
nginx-1.2.6.tar.gz
我已经全部打包到百度云 - [下载地址](http://pan.baidu.com/s/1geW8b5p) - 下载地址如果失效可以联系我

### 依次安装程序包
``` bash
tar -zxvf openssl-fips-2.0.2.tar.gz
cd openssl-fips-2.0.2
./config
make
make install

tar -zxvf zlib-1.2.7.tar.gz
cd zlib-1.2.7
./configure
make
make install

tar -zxvf pcre-8.21.tar.gz
cd pcre-8.21
./configure
make
make install


tar -zxvf nginx-1.2.6.tar.gz
cd nginx-1.2.6
./configure --with-pcre=../pcre-8.21 --with-zlib=../zlib-1.2.7 --with-openssl=../openssl-fips-2.0.2
make
make install
```
执行完成后Nginx就安装成功了

### 检测是否安装成功
``` bash
cd  /usr/local/nginx/sbin
./nginx -t

出现 syntax is ok 表示安装成功
```

### 启动Nginx 和 查看端口
``` bash
//启动
./nginx
//查看端口
netstat -ntlp
```