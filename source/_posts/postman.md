﻿---
title: 💯Postman工具学习使用💯
date: 2017-01-13 11:13:36
tags: postman
---
![postman](https://www.getpostman.com/img/v2/logo-big.svg)
>因为工作的关系，常常写一些 API 供 APP 使用。
以前傻傻的，每次测试的时候都会自己刻一个 HTML 的表单，一个一个填入 input ，接着送出。后来觉得这样太慢了，就用 JavaScript 写了一个程式来送， 可是效率都没有很好，尤其是需要反覆测试更改条件的时候。
之后在同事的推荐下用了 Burpsuite ，而这套软体确实是可以做到没错，但是让人有一种「杀鸡焉用牛刀」的感觉。因此又陆续找了几个模拟 HTTP requests 的工具，却都不甚理想。最近终于找到一套满意的，也就是今天要介绍的 Postman。
Postman 是一个 Chrome 的 Extension

<!--more-->
Postman banner
![postman](https://www.getpostman.com/img/v2/homepage/postman-hero.jpg)
### Postman的主要功能
模拟各种 HTTP requests：从常用的 GET、POST 到 RESTful 的 PUT 、 DELETE …等等。甚至还可以送出档案、送出额外的 header。

Collection 功能：Collection 是 requests的集合，在做完单一个测试的时候， 你可以把这次的 request 存到特定的 Collection 里面，如此一来，下次要测试的时候，就不需要重新输入。
养成习惯以后，网站 API 的每个方法都写好存进去，以后在开发的时候，就可以迅速得看到结果。而 Collection 还可以 Import 或是 Share 出来，让团队里面的其他人，可以一起使用你建立起来的 Collection。

整理过后的回传结果：一般在用其他工具来测试的时候，回传的东西通常都是纯文字的 raw， 但如果是 JSON ，就是塞成一整行的 JSON。这会造成阅读时的障碍 ，而 Postman 可以针对回传资料的格式自动美化。 JSON、 XML 或是 HTML 都会整理成人类可以阅读的型态。

设定环境：Postman 可以自由新增 Environment，一般我们可能会有多种环境， development 、 staging 或 local， 而这几种环境的 request URL 也各不相同。新增 Environment，可以让我们设定一些环境变数，使得切换环境测试的时候， 不用重写 request。

### Postman主要使用
>日常中使用Postman无非就两点，测试API查看数据，写API文档
所以本文就将介绍Postman的使用、团队创建、测试API、保存API和分享API文档

### Postman注册
打开--> [官网](https://www.getpostman.com/)
根据自己的需求下载
我用的是Chrome形式使用Postman
然后注册，填写必要的信息后。注册成功

### 团队的创建
点击官网菜单的[Team](https://app.getpostman.com/dashboard/teams)
可以进行团队创建
创建团队主要用于API的分享

### 测试自己的API
![](https://www.getpostman.com/img/v2/homepage/express-api-development.png?7f4d36d1c8e2329de086161a2ad5f6a2)
在input输入API链接，点击Send，就可以看到自己的数据

### 保存自己的API
1、先创建自己的文件夹
2、测试好API无误后点击Save
3、编辑API接口说明
4、选择自己的文件夹进行保存

### 分享API
1、点击文件夹右边 •••
2、选择Share
3、选择Team进行分享

🍘也可以设置Public公开分享🍙