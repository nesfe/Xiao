﻿---
title: angular路由加载配合🍡nprogress调用
date: 2017-01-11 15:07:29
tags: [angular,nprogress]
---
>本文是上一篇笔记的衍生笔记，没看上一篇的同学请[点击入口](/2017/01/10/webpack-angular/)
本文重点：
1、父Controller形成加载接口
2、子Controller调用接口
3、ng-view显示配合nprogress
4、node路由故意延时数据

<!--more-->
### 增加一个node路由配置
增加一个路由配置，用于获取一个延时数据，用于测试nprogress
``` js
app.get('/data', function (req, res) {
    //故意延时5秒
    setTimeout(function(){
        var data={data:'一条数据'};
        res.end(JSON.stringify(data));
    },5000);
});
```

### webpack打包nprogress
>由于我没有分离css和js,最终会形成一个js文件

``` js
//加载nprogress
window.NProgress = require('./ui/nprogress');
require('../css/nprogress.css');
```

### 增加controller接口
这里添加的是全局父controller
``` js
//全局父controller
app.controller('controller', ['$scope', function ($scope) {
    $scope.info = '显示这条数据的是 index.js 全局';
    //控制菜单的样式高亮显示接口
    $scope.$on('navChange', function (event, msg) {
        $('#nav li').removeClass('active');
        $('#nav li').eq(msg).addClass('active');
    });
    //加载开始
    $scope.$on('NProgress.start', function (event, msg) {
        //开始前.隐藏
        $('#ngView').hide();
        window.NProgress.start();
    });
    //加载结束
    $scope.$on('NProgress.done', function (event, msg) {
        //开始后.显示
        $('#ngView').fadeIn(500);
        window.NProgress.done();
    });
}]);
```

### 添加子controller调用
page_index.js
``` js
var app = angular.module('myApp.index', ['ngRoute']);
app.controller('indexController', ['$scope', function ($scope) {
    //加载开始
    $scope.$emit("NProgress.start");
    //菜单高亮第一个
    $scope.$emit("navChange", 0);
    $scope.title = "双语幼儿园成绩单";
    $scope.Sr = [{
        name: "刘德华",
        age: 6,
        cj: 98
    }, {
        name: "张学友",
        age: 6,
        cj: 64
    }, {
        name: "黎明",
        age: 6,
        cj: 55
    }, {
        name: "郭富城",
        age: 6,
        cj: 78
    }];
    //加载结束
    $scope.$emit("NProgress.done");
}]);
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'indexController',
            templateUrl: 'view/index/page_index.html'
        });
}]);
```
page_list.js
``` js
var app = angular.module('myApp.list', ['ngRoute']);
app.controller('listController', ['$scope', '$http', function ($scope, $http) {
    //加载开始
    $scope.$emit("NProgress.start");
    //菜单高亮第二个
    $scope.$emit("navChange", 1);
    $scope.title = "List 子页面";
    //get请求延时数据
    $http({
        method: 'GET',
        url: '/data'
    }).then(function successCallback(response) {//成功
        $scope.data=response.data;
        //加载结束
        $scope.$emit("NProgress.done");
    }, function errorCallback(response) {
        //失败回调
    });
    //如果很多$http请求，建议自定义回调函数
}]);
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/list', {
            controller: 'listController',
            templateUrl: 'view/index/page_list.html'
        });
}]);
```
### 测试
打开[测试网址8081](http://localhost:8081/)
如果没意外的话，应该能完美运行，并且融合nprogress😘