---
title: Angular2学习笔记（一）模块
date: 2017-03-17 14:17:03
tags: angular2
---
>前言：
Angular2是基于TypeScript开发的，不了解TypeScript可以先去了解下
[TypeScript](https://www.tslang.cn/)

![](https://angular.cn/resources/images/logos/angular2/angular.svg)

<!--more-->
>本次笔记用到的是QuickStart种子
这个QuickStart种子的live example就是QuickStart加上app.module.ts和main.ts文件 (稍后会讲到)，它能让制作更复杂的应用例子更简便。

确定你已经安装了 node 和 npm，然后：

新建项目目录（你可以暂时为其取名quickstart，以后再重命名）。
克隆或者下载《快速起步》种子到你的项目目录。
安装 npm 包。
运行npm start来启动例子应用。
克隆
运行下列命令来执行克隆并启动步骤。
``` bash
git clone https://github.com/angular/quickstart.git quickstart
cd quickstart
npm install
npm start
```

注意/app目录中以下三个 TypeScript (.ts) 文件：

src/app/app.component.ts
``` js
import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1>Hello {{name}}</h1>`
})
export class AppComponent { name = 'Angular'; }
```

src/app/app.module.ts
``` js
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
```

src/main.ts
``` js
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule }              from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
```
<table>
    <tr>
        <td>app/app.component.ts</td>
	<td>它是根组件，随着应用的演变，它将变成一颗嵌套组件树。</td>
    </tr>
    <tr>
        <td>app/app.module.ts</td>
	<td>定义AppModule，根模块为 Angular 描述如何组装应用。 目前，它只声明了AppComponent。 不久，它将声明更多组件。</td>
    </tr>
    <tr>
        <td>main.ts</td>
	<td>使即时 (JiT) 编译器用编译应用并且在浏览器中启动并运行应用。 对于大多数项目的开发，这都是合理的选择。而且它是在像 Plunker 这样的在线编程环境中运行例子的唯一选择。 你将在本文档中学习其他编译和开发选择。</td>
    </tr>
</table>


### Angular 应用的基本构造块
>Angular 是一个用 HTML 和 JavaScript 或者一个可以编译成 JavaScript 的语言（例如 Dart 或者 TypeScript ），来构建客户端应用的框架。
该框架包括一系列库，有些是核心库，有些是可选库。
我们是这样写 Angular 应用的：用 Angular 扩展语法编写 HTML 模板， 用组件类管理这些模板，用服务添加应用逻辑， 用模块打包发布组件与服务。
然后，我们通过引导根模块来启动该应用。 Angular 在浏览器中接管、展现应用的内容，并根据我们提供的操作指令响应用户的交互。
当然，这只是冰山一角。后面我们将学习更多的细节。不过，目前我们还是先关注全景图吧。

![](https://angular.cn/resources/images/devguide/architecture/overview2.png)


### 模块
![](https://angular.cn/resources/images/devguide/architecture/module.png)
Angular 应用是模块化的，并且 Angular 有自己的模块系统，它被称为 Angular 模块或 NgModules。
Angular 模块很重要。这里只是简单介绍，在 Angular 模块中会做深入讲解。
每个 Angular 应用至少有一个模块（根模块），习惯上命名为AppModule。
根模块在一些小型应用中可能是唯一的模块，大多数应用会有很多特性模块，每个模块都是一个内聚的代码块专注于某个应用领域、工作流或紧密相关的功能。
Angular 模块（无论是根模块还是特性模块）都是一个带有@NgModule装饰器的类。.l-sub-section
装饰器是用来修饰 JavaScript 类的函数。 Angular 有很多装饰器，它们负责把元数据附加到类上，以了解那些类的设计意图以及它们应如何工作。 关于装饰器的更多信息。
NgModule是一个装饰器函数，它接收一个用来描述模块属性的元数据对象。其中最重要的属性是：

declarations - 声明本模块中拥有的视图类。 Angular 有三种视图类：组件、指令和管道。
exports - declarations 的子集，可用于其它模块的组件模板。
imports - 本模块声明的组件模板需要的类所在的其它模块。
providers - 服务的创建者，并加入到全局服务列表中，可用于应用任何部分。
bootstrap - 指定应用的主视图（称为根组件），它是所有其它视图的宿主。只有根模块才能设置bootstrap属性。

下面是一个简单的根模块：
``` js
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
  imports:      [ BrowserModule ],
  providers:    [ Logger ],
  declarations: [ AppComponent ],
  exports:      [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
```
>AppComponent的export语句只是用于演示如何导出的，它在这个例子中并不是必须的。 根模块不需要导出任何东西，因为其它组件不需要导入根模块。


我们通过引导根模块来启动应用。 在开发期间，你通常在一个main.ts文件中引导AppModule，就像这样：
``` js
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
```

### Angular 模块库
![](https://angular.cn/resources/images/devguide/architecture/library-module.png)
Angular 提供了一组 JavaScript 模块。可以把它们看做库模块。

每个 Angular 库的名字都带有@angular前缀。

用 npm 包管理工具安装它们，用 JavaScript 的import语句导入其中某些部件。

例如，象下面这样，从@angular/core库中导入Component装饰器：

``` js
import { Component } from '@angular/core';
```

还可以使用 JavaScript 的导入语句从 Angular 库中导入 Angular 模块。
``` js
import { BrowserModule } from '@angular/platform-browser';
```

在上面那个简单的根模块的例子中，应用模块需要BrowserModule的某些素材。 要访问这些素材，就得把它加入@NgModule元数据的imports中，就像这样：
``` js
imports:      [ BrowserModule ],
```

这种情况下，你同时使用了 Angular 和 JavaScript 的模块化系统。
这两个系统比较容易混淆，因为它们共享相同的词汇 “imports” 和 “exports”。 先放一放，随着时间和经验的增长，自然就会澄清了。