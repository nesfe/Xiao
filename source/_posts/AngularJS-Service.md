﻿---
title: AngularJS Service 常见服务 + 五种自定义服务
date: 2017-02-23 14:19:16
tags: angular
---
>AngularJS 服务(Service)
AngularJS 中你可以创建自己的服务，或使用内建服务。
什么是服务？
在 AngularJS 中，服务是一个函数或对象，可在你的 AngularJS 应用中使用。
AngularJS 内建了30 多个服务。

![angular](http://files.jb51.net/file_images/article/201512/201512051321451.png)
<!--more-->
>为什么使用服务?
在很多服务中，比如 $location 服务，它可以使用 DOM 中存在的对象，类似 window.location 对象，但 window.location 对象在 AngularJS 应用中有一定的局限性。
AngularJS 会一直监控应用，处理事件变化， AngularJS 使用 $location 服务比使用 window.location 对象更好。

### $scope作用域
>Scope(作用域) 是应用在 HTML (视图) 和 JavaScript (控制器)之间的纽带。
Scope 是一个对象，有可用的方法和属性。
Scope 可应用在视图和控制器上。
当你在 AngularJS 创建控制器时，你可以将 $scope 对象当作一个参数传递。

``` html
<div ng-app="myApp" ng-controller="myCtrl">
<h1>{{carname}}</h1>
</div>
```
``` js
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {
    $scope.carname = "Volvo";
});
</script>
```

### $http 服务
$http 是 AngularJS 应用中最常用的服务。 服务向服务器发送请求，应用响应服务器传送过来的数据。
``` js
var app = angular.module("myApp", []);
    app.controller("controller", ["$scope", "$http", function ($scope, $http) {
        $scope.http='http';
        $http({
            method: 'GET',
            url: 'http://10.63.64.176:8080/list'
        }).then(function successCallback(response) {
            console.log(response);
        }, function errorCallback(response) {
        });
    }]);
```

### $timeout 服务
AngularJS $timeout 服务对应了 JS window.setTimeout 函数。
``` js
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $timeout) {
    $timeout(function () {
        $scope.myHeader = "2秒可以看见我";
    }, 2000);
});
```

### $interval 服务
AngularJS $interval 服务对应了 JS window.setInterval 函数。
``` js
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $interval) {
    $interval(function () {
        $scope.theTime = new Date();
    }, 1000);
});
```

### $window服务
AngularJS $window 服务对应了 JS window 函数。

### $document服务
AngularJS $window 服务对应了 JS window.document 函数。

### $compile服务
$compile的功能是,将一个html字符串或者一个DOM进行编译,最后返回一个链接函数,这个链接函数可以用于将作用域(Scope)和模版"链接"到一起.编译的过程,其实质是遍历DOM树,匹配和处理DOM上的各种指令的过程.

``` js
app.controller("cr_compile", ["$scope", function ($scope) {
        $scope.cities=['123','456','789'];
    }]).directive("evalExpression", ["$compile",function ($compile) {
        return function (scope, element, attrs) {
            var content = "<ul><li ng-repeat='city in cities'>{{city}}</li></ul>";
            var listEle = angular.element(content);
            var complileFn = $compile(listEle);
            complileFn(scope);
            element.append(listEle);
        }
    }]);
```

### 自定义服务
``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>angular</title>
    <script src="node_modules/angular/angular.js"></script>
</head>
<body ng-app="myApp" ng-controller="controller">

<h1>{{Service1}}</h1>
<h1>{{Service2}}</h1>
<h1>{{Service3}}</h1>
<h1>{{Service4}}</h1>
<h1>{{Service5}}</h1>

<script type="text/javascript">
    var app = angular.module("myApp", []);

    /**
     * Angular Service 自定义模式 1
     * 参数为一个JSON对象
     * 用于存储固定对象
     * */
    app.constant('Service1', {
        name: 'Jia'
    });

    /**
     * Angular Service 自定义模式 2
     * 参数为一个JSON对象
     * 用于存储非固定对象
     * */
    app.value('Service2', {
        name: 'Jia'
    });

    /**
     * Angular Service 自定义模式 3
     * 参数为一个函数
     * factory服务是有一个处理过程,经过这个过程,才返回结果的.
     * 返回一个对象.实际被注入的服务就是这个对象.
     * */
    app.factory('Service3', function () {
        var id = '111';
        var name = '222';
        return {
            getId: function () {
                return id;
            },
            getName: function () {
                return name;
            }
        }
    });

    /**
     * Angular Service 自定义模式 4
     * 参数为一个构造函数
     * 最后被注入的服务是这个构造函数实例化以后的结果.所以基本上使用service创建的服务的,也都可以使用factory来创建.
     * */
    app.service('Service4',function () {
        var name="Service4";
        this.getName=function () {
            return name;
        }
    });

    /**
     * Angular Service 自定义模式 5
     * 参数为一个函数
     * $get方法就相当于factory服务的第二个参数,最后要返回一个对象,这个对象就是真正被注入的服务
     * */
    app.provider('Service5', function(){
        var name="Service5";
        return {
            name:name,
            $get:function(){
                return this.name;
            }
        }
    });

    //控制器
    app.controller("controller", ["$scope",
        "Service1",
        "Service2",
        "Service3",
        "Service4",
        "Service5",
        function ($scope, Service1, Service2, Service3, Service4, Service5) {
            $scope.Service1 = Service1.name;
            $scope.Service2 = Service2.name;
            $scope.Service3 = Service3.getId();
            $scope.Service4 = Service4.getName();
            $scope.Service5 = Service5;
        }]);
</script>
</body>
</html>
```