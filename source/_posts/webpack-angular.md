---
title: webpack 打包 angular bootstrap jquery
date: 2017-01-10 15:09:54
tags: [webpack,angular]
---
>本文主要介绍用**webpack**打包**angular**，包括**angular**组件的合理引用，比如
**angular-route.min.js 路由控件** 等等...
bootstrap and jquery为附加工具
🤷🤷🤷 使用**NodeJs**为**angular路由**后台测试服务器 🤷🤷🤷

<!--more-->
### 开始前的准备工作当然就是安装插件
package.json
``` json
{
  "name": "webpack-angular",
  "version": "1.0.0",
  "description": "",
  "main": "webpack.config.js",
  "scripts": {
    "start": "node app.node",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "angular-route": "^1.6.1",
    "css-loader": "^0.26.1",
    "ejs": "^2.5.5",
    "express-handlebars": "^3.0.0",
    "file-loader": "^0.9.0",
    "jQuery": "^1.7.4",
    "jquery": "^3.1.1",
    "style-loader": "^0.13.1",
    "url-loader": "^0.5.7",
    "webpack": "^1.14.0",
    "webpack-dev-server": "^1.16.2"
  },
  "dependencies": {
    "angular": "^1.6.1",
    "exports-loader": "^0.6.3"
  }
}
```
Run
``` bash
$ npm install
```
### 目录介绍 - 🍔
请仔细看目录结构，**很重要**
![目录结构](http://ohhzjlczd.bkt.clouddn.com/23.png)
介绍
``` bash
dist			<- 存储模板文件夹
dist/view/		<- 存储模板子页面
src			<- js,css编译文件夹
src/js/core		<- 存储模板子页面js文件夹
app.node.js		<- node运行
webpack.config.js	<- webpack运行
```
### webpack - 💡
配置文件 webpack.config.js
``` js
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var path = require('path');
var webpack = require('webpack');
var fs = require('fs');
var uglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var srcDir = path.resolve(process.cwd(), 'src');

//获取js文件夹所有js文件
function getEntry() {
    var jsPath = path.resolve(srcDir, 'js');
    var dirs = fs.readdirSync(jsPath);
    var matchs = [], files = {};
    dirs.forEach(function (item) {
        matchs = item.match(/(.+)\.js$/);
        if (matchs) {
            files[matchs[1]] = path.resolve(srcDir, 'js', item);
        }
    });
    return files;
}
module.exports = {
    cache: true,
    devtool: "source-map",
    entry: getEntry(),
    output: {
        path: path.join(__dirname, "dist/js/"),
        publicPath: "dist/js/",
        filename: "[name].js",
        chunkFilename: "[chunkhash].js"
    },
    module: {
        loaders: [
            {test: /\.css$/, loader: 'style!css'},
            {test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=50000&name=[path][name].[ext]'},
            {
                test: require.resolve(srcDir + '/js/lib/angular.min.js'),
                loader: "exports?window.angular"
            }//对于非模块化的库，可以通过exports插件暴露出全局变量，如angular
        ],
        //配置webpack直接忽略不解析该文件的依赖关系。对于比较大的库很有用，如reactjs.主要提高打包速度。
        noParse: [
            srcDir + '/js/lib/angular.min.js'
        ]
    },
    resolve: {
        alias: {
            angular: srcDir + "/js/lib/angular.min.js",
            jquery: srcDir + "/js/lib/jquery.min.js",
            core: srcDir + "/js/core",
            ui: srcDir + "/js/ui"
        }
    },
    plugins: [
        //提供全局的变量，在模块中使用无需用require引入
        new webpack.ProvidePlugin({
            angular: "angular",
            jquery: "jquery",
            jQuery:"jquery",
            $: "jquery"
        }),
        //相同代码打包成common.js
        new CommonsChunkPlugin('common.js'),
        new uglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};
```

### 创建angular路由模板、模板子页面、控制JS
src -> js -> index.js //模板控制JS
``` js
//赋值后,页面就可以用$
window.$ = $;
//加载bootstrap
require('../css/bootstrap.min.css');
require('./ui/bootstrap.min.js');

//加载angular路由控件
require('./ui/angular-route.min');

//全局点击事件
$('a[abs]').each(function () {
    var href = $(this).attr('href');
    $(this).attr('href', 'javascript:;');
    $(this).click(function () {
        window.location = href;
    });
});

var app = angular.module('myApp', ['myApp.index', 'myApp.list', 'ngRoute']);
app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    //去除#访问
    $locationProvider.html5Mode(true);
}]);

//全局controller
app.controller('controller', ['$scope', function ($scope) {
    $scope.info = '显示这条数据的是 index.js 全局';
}]);

//加载 /index 路由信息
require('./core/index/page_index');
//加载 /list 路由信息
require('./core/index/page_list');
```
src -> js -> core -> index -> * //子页面控制JS
``` js
//page_index.js
var app = angular.module('myApp.index', ['ngRoute']);
app.controller('indexController', ['$scope', function ($scope) {
    $('#nav li').removeClass('active');
    $('#nav li').eq(0).addClass('active');
    $scope.title = "双语幼儿园成绩单";
    $scope.Sr = [{
        name: "刘德华",
        age: 6,
        cj: 98
    }, {
        name: "张学友",
        age: 6,
        cj: 64
    }, {
        name: "黎明",
        age: 6,
        cj: 55
    }, {
        name: "郭富城",
        age: 6,
        cj: 78
    }]
}]);
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'indexController',
            templateUrl: 'view/index/page_index.html'
        });
}]);



//page_list.js
var app = angular.module('myApp.list', ['ngRoute']);
app.controller('listController', ['$scope', function ($scope) {
    $scope.title = "List 子页面";
    $('#nav li').removeClass('active');
    $('#nav li').eq(1).addClass('active');
}]);
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/list', {
            controller: 'listController',
            templateUrl: 'view/index/page_list.html'
        });
}]);
```

dist -> index.html  //创建模板
``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>webpack-angular</title>
    <!--href 属性规定页面中所有相对链接的基准 URL。-->
    <base href="/" /><!-- 去除路由带#号 -->
</head>
<body ng-app="myApp" ng-controller="controller">

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Webpack-Angular</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" id="nav">
                <li><a href="/">Home</a></li>
                <li><a href="/list">List</a></li>
                <!--这里的标签加了abs证明这里的链接不需要angular路由-->
                <li><a href="/main" abs>Main</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>
                    {{ info }}
                </h3>
            </div>
        </div>
        <div class="row">
            <!--子页面切换入口-->
            <div class="col-xs-12" ng-view>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
</body>
</html>
```
dist -> view -> index -> *  //创建模板子页面
``` html
//page_index.html
<h4>
    {{ title }}
</h4>
<table class="table table-bordered table-hover">
    <tr>
        <th>名字</th>
        <th>年龄</th>
        <th>成绩总分</th>
    </tr>
    <tr ng-repeat="x in Sr">
        <td>{{ x.name }}</td>
        <td>{{ x.age }}</td>
        <td>{{ x.cj }}</td>
    </tr>
</table>
	
	
//page_list.html
{{ title }}
```

这样就是webpack-angular全部配置
但是还需要服务器的路由转发来让angular跑起来
这里我用的是Node

### Node服务器 - 🎌
很简单，一个js文件即可
app.node.js
``` js
var express = require('express');
var path = require('path');
var ejs = require('ejs');
var app = express();

//设置静态文件夹
app.use(express.static(path.join(__dirname, 'dist')));
//设置模板文件夹
app.set('views', path.join(__dirname, 'dist'));
app.engine('.html', ejs.__express);
app.set('view engine', 'html');

//路由  /   需要跟angular一致
app.get('/', function (req, res) {
    res.render('index')
});
//路由  /list   需要跟angular一致
app.get('/list', function (req, res) {
    res.render('index')
});
//其他路由
app.get('/main', function (req, res) {
    res.render('main')
});
var server = app.listen(8081, function () {
    console.log('open  -->  http://localhost:8081/');
});
```
Run - 运行
``` bash
$ node app.node.js
```
如果运气好的话，打开[测试网址8081的端口](http://localhost:8081/)
就可以看到效果，Good luck 😃