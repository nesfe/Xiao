---
title: IconPreview
date: 2017-02-27 17:38:14
tags: icon
---
>用于快速预览favicon.ico的小工具
使用方法：直接拷贝Code到一个html文档即可

![icon](http://ohhzjlczd.bkt.clouddn.com/bg.png)
<!--more-->
### Icon Preview
图标预览

Code：
``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Icon Preview</title>
    <link href='//cdn.webfont.youziku.com/webfonts/nomal/99006/28683/58af9292f629d804b420d0a5.css' rel='stylesheet' type='text/css' />
    <style type="text/css">
        .h1 {
            text-align: center;
            font-family:'AbrazoScriptSSi912fdd46b182be';
            font-size: 50px;
            margin-top: 30px;
        }
        .input{
            text-align: center;
            padding-top: 20px;
        }
        .input input{
            width: 300px;
            height: 40px;
            font-size: 14px;
            padding:0 10px;
            line-height: 40px;
            margin-bottom: 10px;
            color: #fff;
            background-color: #2baaeb;
            border: 0;
            box-shadow: 0 4px 0 #2694cc;
            transition-duration: .3s;
        }
        .input input:hover{
            background-color: #2694cc;
            line-height: 44px;
        }
        *{
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }
        .img{
            width: 95%;
            height: 3000px;
            background-image: url("http://ohhzjlczd.bkt.clouddn.com/bg.png");
            background-position: center top;
            background-repeat: no-repeat;
            background-size: 100% auto;
            margin: 0 auto;
            position: relative;
        }
        .image{
            padding-top: 40px;
        }
        .icon{
            position: absolute;
            width: 1.5625%;
            height: 2.370037%;
            background-color: #f2f2f2;
            top: 1.35%;
            left: 2.1484375%;
            border: 0;
        }
    </style>
</head>
<body>
<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.js"></script>
<h1 class="h1">Icon Preview</h1>
<div class="input">
    <input type="button" value="选 择 图 标" id="button" style="cursor: pointer">
    <input type="file" id="file" style="display: none">
</div>

<div class="image">
    <div class="img" id="imgBg">
        <img src="" class="icon" id="img" alt="">
    </div>
</div>
<script type="text/javascript">
    $('#button').click(function () {
        $('#file').click();
    });
    $("#file").change(function(){
        var objUrl = getObjectURL(this.files[0]) ;
        if (objUrl) {
            $("#img").attr("src", objUrl) ;
        }
    }) ;
    function getObjectURL(file) {
        var url = null ;
        if (window.createObjectURL!=undefined) { // basic
            url = window.createObjectURL(file) ;
        } else if (window.URL!=undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file) ;
        } else if (window.webkitURL!=undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file) ;
        }
        return url ;
    }
    window.onload=function () {
        var w=$('#imgBg').width();
        $('#imgBg').height(1350/2048*w);
    }
</script>
</body>
</html>
```