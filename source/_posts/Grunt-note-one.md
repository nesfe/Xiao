﻿---
title: Grunt工具学习笔记[1] - Grunt基础入门
date: 2017-01-04 16:14:08
tags: [grunt,less]
---
### Grunt是什么？
>Grunt是基于Node.js的项目构建工具。它可以自动运行你所设定的任务。Grunt拥有数量庞大的插件，几乎任何你所要做的事情都可以用Grunt实现。

<!--more-->
### 为何要用Grunt构建工具？
>一句话：自动化。对于需要反复重复的任务，例如压缩（minification）、编译、单元测试、linting等，自动化工具可以减轻你的劳动，简化你的工作。当你在 Gruntfile 文件正确配置好了任务，任务运行器就会自动帮你或你的小组完成大部分无聊的工作。
Grunt生态系统非常庞大，并且一直在增长。由于拥有数量庞大的插件可供选择，因此，你可以利用Grunt自动完成任何事，并且花费最少的代价。如果找不到你所需要的插件，那就自己动手创造一个Grunt插件，然后将其发布到npm上吧。先看看入门文档吧。

### 可用的Grunt插件
你所需要的大多数task都已经作为Grunt插件被开发了出来，并且每天都有更多的插件诞生。插件列表页面列出了完整的清单。下面给出几个你可能听说过的插件：
![](http://www.gruntjs.net/img/logo-coffeescript.jpg)
![](http://www.gruntjs.net/img/logo-handlebars.jpg)
![](http://www.gruntjs.net/img/logo-less.jpg)

等等等... [MORE](http://www.gruntjs.net/plugins)

### Grunt官网 <i class="fa fa-globe"></i>
[Grunt](http://www.gruntjs.net/)

### 安装Grunt
``` bash
npm install -g grunt-cli
```

### 安装本文用到的插件
package.json
``` json
{
  "name": "grunt",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "grunt": "^0.4.5",
    "grunt-contrib-cssmin": "^1.0.2",
    "grunt-contrib-jshint": "^0.10.0",
    "grunt-contrib-less": "^1.4.0",
    "grunt-contrib-nodeunit": "~0.4.1",
    "grunt-contrib-uglify": "^0.5.1",
    "grunt-contrib-watch": "^1.0.0"
  }
}

```
``` bash
npm install
```
### 创建Gruntfile.js
``` js
/**
 * Grunt 工具学习笔记（一）
 * */
module.exports = function (grunt) {
    //配置方法
    grunt.initConfig({
        //获取配置文件
        pkg: grunt.file.readJSON('package.json'),
        //js压缩
        uglify: {
            options: {
                //头部生成
                banner: '/*! 自动生成 <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                files:[
                    {
                        //相对路径
                        expand:true,
                        cwd:'src/js/',
                        src:'*.js',
                        dest:'dest/js/'
                    }
                ]
            }
        },
        //less插件编译
        less: {
            build: {
                options: {
                    paths: ["src/"],  // @import 加载文件的路径
                    modifyVars: {   // 重新定义全局变量
                        imgPath: '"http://baidu.com/"',
                        bgColor: 'red'
                    }
                },
                files: {
                    //将 app.less 编译为 app.css
                    "src/css/app.css": "src/css/app.less"
                }
            }
        },
        //css压缩
        cssmin:{
            options:{
                banner: '/*! 自动生成 <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                //美化
                beautify:{
                    ascii_only: true
                }
            },
            build:{
                files:[
                    {
                        //相对路径
                        expand:true,
                        cwd:'src/css',
                        src:'app.css',
                        dest:'dest/css'
                    }
                ]
            }
        },
        //监听事件
        watch:{
            build:{
                files:['src/js/*.js','src/css/*.css'],
                tasks:['less','cssmin','uglify'],
                options:{spawn:false}
            }
        }
    });

    // 加载一些一目了然的插件。
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    //默认被执行的任务列表。
    grunt.registerTask('default', ['contrib']);

    grunt.registerTask('contrib', [
        'less',     // less编译
        'cssmin',   // css压缩
        'uglify',   // js压缩
        'watch'     // 文件监听
    ]);
};
```
### 目录结构
``` bash
.
├── src
  ├── css
    ├── app.less
  ├── js
    ├── app.js
├── Gruntfile.js
├── package.json
```

### 执行命令
``` bash
grunt
```

执行后，会自动生成dest文件夹
文件内包括压缩好的css and js

### END
✌✌✌✌✌