﻿---
title: Grunt工具学习笔记[2] - 用webpack代替uglify打包js
date: 2017-01-05 10:43:05
tags: [grunt,webpack]
---
### 介绍
>本文是接着上一文继续探索，用webpack代替uglify打包js
没看上文的小伙伴，先请移步 [上一文](/2017/01/04/Grunt-note-one/)

<!--more-->
### webpack安装
既然要融合当然需要安装 grunt-webpack
但是grunt-webpack 依赖于 webpack
所以
``` bash
npm install webpack grunt-webpack --save-dev
安装完成
```
### 处理Task事件
webpack
``` js
webpack: {
            someName: {
                // webpack 选项 这里必须加 ./ 不加则报错，不知为何
                entry: ["./src/js/app.js","./src/js/grunt.js"],//入口
                // 把 app.js grunt.js 打包成一个js
                output: {
                    path: "./dest/js/",
                    filename: "main.js"//出口
                },

                stats: {
                    // 配置控制台输出
                    colors: false,
                    modules: true,
                    reasons: true
                },
                // stats: false 为禁用 否则为输出

                storeStatsTo: "xyz", // writes the status to a variable named xyz
                // you may use it later in grunt i.e. <%= xyz.hash %>

                progress: false, // 不显示progress
                //默认为true

                failOnError: false, // 不报告错误，如果 webpack找到错误
                // Use this if webpack errors are tolerable and grunt should continue

                watch: true, // 使用 webpacks 监听
                // 需要保存进程

                watchOptions: {
                    aggregateTimeout: 500,
                    poll: true
                },
                // Use this when you need to fallback to poll based watching (webpack 1.9.1+ only)

                keepalive: true, // 不完成 grunt task
                // 默认为true 使用 watch 或者 dev-server ，如果都没有为 false
                // defaults to true for watch and dev-server otherwise false

                inline: true,  // 载入 webpack-dev-server 运行时包
                // Defaults to false 默认为false

                hot: true // adds the HotModuleReplacementPlugin and switch the server to hot mode
                // Use this in combination with the inline option

            },
            anotherName: {}
        }
```

### 运行命令
``` bash
grunt
```

### 结束
END
