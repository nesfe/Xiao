---
title: 使用 Gulp 流式化 Less 编译 Sprite 雪碧图
date: 2016-12-30 14:06:19
tags: [gulp,less,sprite]
---
大家好，本文用于记录Gulp-Less-Sprite一体化
> 注意：本文需要对Gulp有一定了解，不了解的同学可以[Gulp](http://www.gulpjs.com.cn/) 。

话不多说 😊😊😊
先来看看我的文件格局
<!--more-->
![](http://ohhzjlczd.bkt.clouddn.com/1312.png?imageView2/2/w/300/interlace/0/q/100)
很简单，没什么可说的
由于使用的是Gulp自动化 就需要一系列Gulp插件
本文使用到的插件有
>gulp -- [demo](https://www.npmjs.com/package/gulp)
gulp-concat -- [demo](https://www.npmjs.com/package/gulp-concat)
gulp-css-spriter -- [demo](https://www.npmjs.com/package/gulp-css-spriter)
gulp-less -- [demo](https://www.npmjs.com/package/gulp-less)
gulp-minify-css -- [demo](https://www.npmjs.com/package/gulp-minify-css)

安装
``` bash
npm install gulp gulp-concat gulp-css-spriter gulp-less gulp-minify-css --save
```
gulpfile.js 文件配置
``` js
var gulp = require('gulp');
var gulpLess = require('gulp-less');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var spriter = require('gulp-css-spriter');
//一系列插件引入

//less > css
gulp.task('less', function () {
    return gulp.src('./src/css/app.less')
        .pipe(gulpLess())
        .pipe(gulp.dest('./src/css'));
});
//雪碧图操作
gulp.task('sprite', function () {
    var timestamp = +new Date();
    return gulp.src('./src/css/app.css')
        .pipe(spriter({
            //合并图片保存路径
            'spriteSheet': './src/images/sprite/sprite.png',
            //替换CSS文件路径
            'pathToSpriteSheetFromCSS': '../images/sprite/sprite.png'
        }))
        .pipe(minifyCSS())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./src/css'));
});
//执行
gulp.task('default', [ 'less','sprite']);
```
然后命令行输入
``` bash
gulp
```
就会把 app.less 编译成 app.css
app.css 执行雪碧图操作，把所有使用到的图片合成一张图sprite.png，保存到指定路径 ，然后 app.css 压缩成 app.min.css

### 以上这就是大致写法
如果我想某些图片执行雪碧图操作，就可以加后缀?_spriter

app.less
``` css
.ico1{
  height: 64px;
  width: 100px;
  background:url(../images/img/1.png?_spriter) 0 0 no-repeat;
}
.ico2{
  height: 64px;
  width: 100px;
  background:url(../images/img/2.png?_spriter) 0 0 no-repeat;
}
.ico3{
  height: 64px;
  width: 100px;
  background:url(../images/img/3.png) 0 0 no-repeat;
}
```
可以看出我在后缀加了?_spriter
主要是用于区别特定的图片执行雪碧图操作
没加后缀则不执行
如果要这样来写还需要修改gulp-css-spriter插件文件
``` bash
路径：
node_modules /
gulp-css-spriter /
lib /
map-over-styles-and-transform-background-image-declarations.js
```
修改
``` js
function mapOverStylesAndTransformAllBackgroundImageDeclarations(styles, cb) {
    var transformedStyles = extend(true, {}, styles);
    transformedStyles.stylesheet.rules.map(function (rule, ruleIndex) {
        if (rule.type === 'rule') {
            rule.declarations = transformMap(rule.declarations, function (declaration, declarationIndex, declarations) {
                var transformedDeclaration = extend(true, {}, declaration);
                transformedDeclaration = attachInfoToDeclaration(declarations, declarationIndex);
                //background-imagealwayshasaurl且判断url是否有?__spriter后缀
                if (transformedDeclaration.property === 'background-image' && /\?_spriter/i.test(transformedDeclaration.value)) {
                    transformedDeclaration.value = transformedDeclaration.value.replace('?_spriter', '');
                    return cb(transformedDeclaration, declarationIndex, declarations);
                }
                //Backgroundisashorthandpropertysomakesure`url()`isinthere且判断url是否有?__spriter后缀
                else if(transformedDeclaration.property === 'background' && /\?_spriter/i.test(transformedDeclaration.value))
                {
                    transformedDeclaration.value = transformedDeclaration.value.replace('?_spriter', '');
                    var hasImageValue = spriterUtil.backgroundURLRegex.test(transformedDeclaration.value);
                    if (hasImageValue) {
                        return cb(transformedDeclaration, declarationIndex, declarations);
                    }
                }
                return {
                    'value': transformedDeclaration
                };
            });
        }
        return rule;
    });
    return transformedStyles;
}
```
这样修改完成之后
以后执行雪碧图操作就会判断有后缀的图片

最后附上 index.html
``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gulp-Less-Sprite</title>
    <link rel="stylesheet" href="css/app.min.css">
</head>
<body>
    <div class="ico1"></div>
    <div class="ico2"></div>
    <div class="ico3"></div>
</body>
</html>
```