var Client = require('ftp');
var colors = require("colors");
var path = require('path');
var fs = require('fs');
var c = new Client();

//FTP账号
var user = {
    host: 'bxu2442200377.my3w.com',
    user: 'bxu2442200377',
    password: 'lk1029131145'
};

c.on('ready', function () {
    /**
     * 遍历FTP服务器上的文件
     * */
    // c.list(function(err, list) {
    //     if (err) throw err;
    //     console.dir(list);
    //     c.end();
    // });

    /**
     * 下载FTP服务器上的文件
     * */
    // c.get('/htdocs/atom.xml', function(err, stream) {
    //     if (err) throw err;
    //     stream.once('close', function() {
    //         console.log('下载完毕');
    //         c.end();
    //     });
    //     stream.pipe(fs.createWriteStream('./htdocs/atom.xml'));
    // });

    /**
     * 上传FTP服务器上的文件
     * */
    // c.put('/op/1.txt', '/htdocs/aa/1.txt', function(err) {
    //     if (err) throw err;
    //     c.end();
    // });


    /**
     * 上传文件夹
     * */
    UploadAll('/public', '/htdocs');

});

function UploadAll(dir, ftpDir) {
    var myDir = dir;
    if (dir.indexOf('./') === -1)
        myDir = '.' + dir;
    if (!fsExistsSync(myDir)) {
        console.log("本地文件夹错误".red);
        c.end();
        return;
    }

    c.mkdir(ftpDir, true, function (err) {
        if (err) {
            console.error(err);
        }
    });

    var files = fs.readdirSync(myDir);
    for (var fn in files) {
        var name = files[fn];
        var fname = myDir + path.sep + name;
        var stat = fs.lstatSync(fname);
        if (stat.isDirectory() == true) {
            UploadAll(myDir + "/" + name, ftpDir + "/" + name);
        }
        else {
            (function (myDir, ftpDir, name) {
                c.put(myDir + "/" + name, ftpDir + "/" + name, function (err) {
                    if (err) console.error(err);
                    console.log("Upload File：".green + (myDir + "/" + name));
                    c.end();
                });
            })(myDir, ftpDir, name);
        }
    }

}


//判断是否存在文件或文件夹
function fsExistsSync(path) {
    try {
        fs.accessSync(path, fs.F_OK);
    } catch (e) {
        return false;
    }
    return true;
}

//上传
c.connect(user);